Abate  v  subside or moderate.
Aberrant  adj  abnormal or deviant
Abeyance  n  suspended action
Abscond  v  depart secretly and hide
Abstemious  adj  sparing in eating and drinking temperate
Admonish  v  warn; reprove.
Adulterate  v  make impure by adding inferior or tainted substances
Aesthetic  artistic dealing with or capable of appreciating the beautiful
Aggregate  v  gather; accumulate
Alacrity  n  cheerful promptness; eagerness
Alleviate  v  relieve
Amalgamate  v  combine; unite in one body
Ambiguous  adj  unclear or doubtful in meaning
Ambivalence  n  the state of having contradictory or conflicting emotional attitudes
Ameliorate  v  improve
Anachronism  n  something or someone misplaced in time
Analogous  adj  comparable
Anarchy  n  absence of governing body; state of disorder
Anomalous  adj  abnormal; irregular
Antipathy  n  aversion; dislike
Apathy  n - lack of caring; indifference
Appease  v  pacify or soothe; relieve
Apprise  v - inform
Approbation  n  approval
Appropriate  acquire; take possession of for ones own use
Arduous  adj  hard; strenuous
Artless  adj  without guile; open and honest
Ascetic  adj  practicing self-denial; austere
Assiduous  adj- diligent
Assuage  v  ease or lessen (pain); satisfy (hunger); soothe (anger)
Attenuate  v  make thinner; weaken or lessen (in density, force, degree)
Audacious  adj  daring bold
Austere  forbiddingly stern; severely simple and unornamented
Autonomous  self-governing
Aver  assert confidently or declare; as used in law, state, formally as a fact
Banal  hackneyed; commonplace; trite; lacking originality
Belie  contradict; give a false impression
Beneficent  kindly; doing good
Bolster  support; reinforce
Bombastic  pompous; using inflated language
Boorish  rude; insensitive
Burgeon  grow forth
Burnish  make shiny by rubbing; polish
Buttress  support; prop up
Cacophonous  discordant; inharmonious
Capricious  unpredictable; fickle
Castigation-punishment; severe criticism
Catalyst  agent that influences the pace of a chemical reaction while it remains unaffected and unchanged; person or thing that causes action
Caustic  burning; sarcastically biting
Chicanery  trickery; deception
Coagulate  thicken; congeal; clot
Coda  concluding section of a musical or literary composition; something that rounds out, summarizes, or concludes
Cogent  convincing
Commensurate  corresponding in extent, degree, amount; proportionate
Compendium  brief; comprehensive summary
Complaisant  trying to please; overly polite; obliging
Compliant  yielding; conforming to requirements
Conciliatory  reconciling; soothing
Condone  overlook; forgive; give tacit approval; excuse
Confound  confuse; puzzled
Connoisseur  person competent to act as a judge of art
Contention  claim; thesis
Contentious  quarrelsome
Contrite  penitent; repentant
Conundrum  riddle; difficult problem
Converge  approach; tend to meet; come together
Convoluted  coiled around; involved; intricate
Craven  cowardly
Daunt  intimidate; frighten
Decorum  propriety; orderliness and good taste in manners
Default  failure to act
Deference  courteous regard for anothers wish
Delineate  portray; depict; sketch
Denigrate  blacken
Deride  ridicule; make fun of
Derivative; unoriginal; obtained from another source
Desiccate  dry up
Desultory  aimless; haphazard; digressing at random
Deterrent  something that discourage; hindrance
Diatribe  bitter scolding; invective
Dichotomy  split; branching into two parts (usually contradictory)
Diffidence  shyness
Diffuse  wordy; rambling; spread out
Digression  wandering away from the subject
Dirge  lament with music
Disabuse  correct a false impression; undeceive
Discerning  mentally quick and observant; having insight
Discordant  not harmonious; conflicting
Discredit  defame; destroy confidence in; disbelieve
Discrepancy  lack of consistency; difference
Discrete  separate; unconnected; consisting of distinct parts
Disingenuous  lacking genuine candor; insincere
Disinterested; unprejudiced
Disjointed  lacking coherence; separated at the joints
Dismiss  eliminate from consideration; reject
Disparage  belittle
Disparate  basically different; unrelated
Dissemble  disguise; pretend
Disseminate  distribute; spread; scatter
Dissolution  disintegration; looseness in morals
Dissonance  discord; opposite of harmony
Distend  expend; swell out
Distill  purify; refine; concentrate
Diverge  vary; go in different directions from the same point
Divest  strip; deprive
Document  provide written evidence
Dogmatic  opinionated; arbitrary; doctrinal
Dormant  sleeping; lethargic; latent
Dupe  someone easily fooled
Ebullient  showing excitement; overflowing with enthusiasm
Eclectic  selective; composed of elements drawn from disparate sources
Efficacy  power to produce desired effect
Effrontery  impudence; shameless boldness; sheer nerve; presumptuousness
Elegy  poem or song expressing lamentation
Elicit  draw out by discussion
Embellish  adorn; ornament; enhance, as a story
Empirical  based on experience
Emulate  imitate; rival
Endemic  prevailing among a specific group of people or in a specific area or country
Enervate  weaken
Engender  cause; produce
Enhance  increase; improve
Ephemeral  short-lived; fleeting
Equanimity  calmness of temperament; composure
Equivocate  lie; mislead; attempt to conceal the truth
Erudite  learned; scholarly
Esoteric  hard to understand; known only to the chosen few
Eulogy  expression of praise, often on the occasion of someones death
Euphemism  mild expression in place of an unpleasant one
Exacerbate  worsen; embitter
Exculpate  clear from blame
Exigency  urgent situation; pressing needs or demands; state of requiring immediate attention
Extrapolation  projection; conjecture
Facetious  joking (often inappropriately)
Facilitate  help bring about; make less difficult
Fallacious  false; misleading
Fatuous  brainless; inane; foolish, yet smug
Fawning  trying to please by behaving obsequiously, flattering, or cringing
Felicitous  apt; suitably expressed; well chosen
Fervor  glowing ardor; intensity of feeling
Flag  droop; grow feeble
Fledgling  inexperienced
Flout  reject; mock; show contempt for
Foment  stir up; instigate
Forestall  prevent by taking action in advance
Frugality  thrift, economy
Futile  useless; hopeless; ineffectual
Gainsay  deny
Garrulous  loquacious; wordy; talkative
Goad  urge on
Gouge  overcharge
Grandiloquent  pompous; bombastic; using high-sounding language
Gregarious  sociable
Guileless  without deceit
Gullible  easily deceived
Harangue  long, passionate, and vehement speech
Homogeneous  off the same kind
Hyperbole  exaggeration; overstatement
Iconoclastic  attacking cherished traditions
Idolatry  worship of idols
Immutable  unchangeable
Impair  injure; hurt
Impassive  without feeling; imperturbable; stoical
Impede  hinder; block
Impermeable  impervious; not permitting passage through its substance
Imperturbable  calm placid
Impervious  impenetrable; incapable of being damaged or distressed
Implacable  incapable of being pacified
Implicit  understood but not stated
Implode  burst inward
Inadvertently  unintentionally; by oversight; carelessly
Inchoate  recently begun; rudimentary; elementary
Incongruity  lack of harmony; absurdity
Inconsequential  insignificant; unimportant
Incorporate  introduce something into a larger whole; combine; unite
Indeterminate  uncertain; not clearly fixed; indefinite
Indigence  poverty
Indolent  lazy
Inert  inactive; lacking power to move
Ingenuous  naοve and trusting; young; unsophisticated
Inherent  firmly established by nature or habit
Innocuous  harmless
Insensible  unconscious; unresponsive
Insinuate  hint; imply; creep in
Insipid  lacking in flavor; dull
Insularity  narrow-mindedness; isolation
Intractable  unruly; stubborn; unyielding
Intransigence  refusal of any compromise; stubbornness
Inundate  overwhelm; flood; submerge
Inured  accustomed; hardened
Invective  abuse
Irascible  irritable; easily angered
Irresolute  uncertain how to act; weak
Itinerary  plan of a trip
Laconic  brief and to the point
Lassitude  languor; weariness
Latent  potential but undeveloped; dormant; hidden
Laud  praise
Lethargic  drowsy; dull
Levee  earthen or stone embankment to prevent flooding
Levity  lack of seriousness or steadiness; frivolity
Log  record of a voyage or flight; record of day-to-day activities
Loquacious  talkative
Lucid  easily understood
Luminous  shining; issuing light
Magnanimity  generosity
Malingerer  one who feigns illness to escape duty
Malleable  capable to being shaped by pounding; impressionable
Maverick  rebel; nonconformist
Mendacious  lying; habitually dishonest
Metamorphosis  change of form
Meticulous  excessively careful; painstaking; scrupulous
Misanthrope  one who hates mankind
Mitigate  appease; moderate
Mollify  soothe
Morose  ill-humored; sullen; melancholy
Mundane  worldly as opposed to spiritual; everyday
Negate  cancel out; nullify; deny
Neophyte  recent convert; beginner
Obdurate  stubborn
Obsequious  slavishly attentive; servile; sycophantic
Obviate  make unnecessary; get rid of
Occlude  shut; close
Officious  meddlesome; excessively pushy in offering ones services
Onerous  burdensome
Opprobrium  infamy; vilification
Oscillate  vibrate pendulumlike; waver
Ostentatious  showy; pretentious; trying to attract attention
Paragon  model of perfection
Partisan  one-sided; prejudiced; committed to a party
Pathological  pertaining to disease
Paucity  scarcity
Pedantic  showing off learning; bookish
Penchant  strong inclination; liking
Penury  severe poverty; stinginess
Perennial  something long-lasting
Perfidious  treacherous; disloyal
Perfunctory  superficial; not thorough; lacking interest, care, enthusiasm
Permeable  penetrable; porous; allowing liquids or gas to pass through
Pervasive  spread throughout
Phlegmatic  clam; not easily disturbed
Piety  devoutness; reverence for God
Placate  pacify; conciliate
Plasticity  ability to be molded
Platitude  trite remark
Plethora  excess; overabundance
Plummet  fall sharply
Porous  full of pores; like a sieve
Pragmatic  practical (as opposed to idealistic); concerned with the practical worth or impact of something
Preamble  introductory statement
Precarious  uncertain; risky
Precipitate  rash; premature; hasty; sudden
Precursor  forerunner
Presumptuous  arrogant; taking liberties
Prevaricate  lie
Probity  uprightness; incorruptibility
Problematic  doubtful; unsettled; questionable
Prodigal  wasteful; reckless with money
Profound  deep; not superficial; complete
Prohibitive  tending to prevent the purchase or use of something
Proliferate  grow rapidly; spread; multiply
Propensity  natural inclination
Propitiate  appease
Propriety 0 fitness; correct conduct
Proscribe  ostracize; banish; outlaw
Pungent  stinging; sharp in taste or smell; caustic
Qualified  limited; restricted
Quibble - minor objection or complaint
Quiescent  at rest; dormant; temporarily inactive
Rarefied  made less dense (of a gas)
Recalcitrant  obstinately stubborn; determined to resist authority; unruly
Recant  disclaim of disavow; retract a precious statement
Recluse  hermit; loner
Recondite  abstruse; profound; secret
Refractory  stubborn, unmanageable
Refute  disprove
Relegate  banish to an inferior position; delegate; assign
Reproach  express disapproval or disappointment
Reprobate  person hardened in sin; devoid of a sense of decency
Repudiate - disown; disavow
Rescind  cancel
Resolution  determination
Resolve  determination; firmness of purpose
Reticent  reserved; uncommunicative; inclined to silence
Reverent  respectful; worshipful
Sage  person celebrated for wisdom
Salubrious  healthful
Sanction  approve; ratify
Satiate  satisfy fully
Saturate  soak thoroughly
Savor  enjoy; have a distinctive flavor, smell, or quality
Secrete  hide away or cache; produce and release a substance into an organism
Shard  fragment, generally of pottery
Skeptic  doubter; person who suspends judgment until having examined the evidence supporting a point of view
Solicitous  worried, concerned
Soporific  sleep-causing; marked by sleepiness
Specious  seemingly reasonable but incorrect; misleading (often intentionally)
Spectrum  color band produced when a beam of light passes through a prism
Sporadic  occurring irregularly
Stigma  token of disgrace; brand
Stint  be thrifty; set limits
Stipulate  make express conditions, specify
Stolid  dull; impassive
Striated  marked with parallel bands; grooved
Strut  pompous walk
Strut  supporting bar
Subpoena  writ summoning a witness to appear
Subside  settle down; descend; grow quiet
Substantiate  establish by evidence; verity; support
Supersede  cause to be set aside; replace; make obsolete
Supposition  hypothesis; surmise
Tacit  understood; not put into words
Tangential  peripheral; only slightly connected; digressing
Tenuous  thin; rare; slim
Tirade  extended scolding; denunciation; harangue
Torpor  lethargy; sluggishness; dormancy
Tortuous  winding; full of curves
Tractable  docile; easily managed
Transgression  violation of a law; sin
Truculence  aggressiveness; ferocity
Vacillate  waver; fluctuate
Venerate  revere
Veracious  truthful
Verbose  wordy
Viable  practical or workable; capable of maintaining life
Viscous  sticky, gluey
Vituperative  abusive; scolding
Volatile  changeable; explosive; evaporating rapidly
Warranted  justified; authorized
Wary  very cautious
Welter  turmoil; bewildering jumble
Whimsical  capricious; fanciful
Zealot  fanatic; perso who shows excessive zeal (enthusiasm)
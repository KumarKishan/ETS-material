0105
1.A: So are you going to see the student play tonight? 
B: I doubt it. I'm still getting over the flu. 
Q: What does the man mean? 
2.A; Gordom needs to find another place to live. The apartment he rents now  
has been sold to a new owner. 
B: He'd better start looking right away. When all the students come back in  
a few weeks, he won't find any near the campus. 
Q: What does the woman suggest Gorden do? 
3.A: Mind if I borrow ur Spanish workbook? 
B: Not as long as I have it back in time to take to class this evening. 
Q: What does the man mean? 
4. A: U know that quiz we took in Dr. Turner's class today? Did u know that  
she was going to give it to us? 
Bl Actually I was just as surprised as u were. 
Q: What does the woman mean? 
5. A: I don't know which color folder to use, white or brown? 
B: What difference does it make? It's the content that's important. 
Q: What does the man mean?  
6. A: U know I heard that professor Martin's introduction to chem class is  
way to demanding for first year students. They say it's as hard as courses  
for graduate students. 
B: Yeah, but a lot of students will tell u otherwise. To talk to anyone  
who's gone on to the advanced course,like organic chem or who study chem in  
graduate school. They r really glad they started out with professor Martin. 
Q: What does the man imply about professor Martin? 
7.A: Hey Mark, have u been able to sell ur old piano yet? 
B: Ah, u were right, just posting notices on bulletin boards at a coupke of  
supermarket wasn't enough. I think i'll have to place an advertisement in  
the local newspaper. 
Q: What does the man imply? 
8.A: My back has been aching ever since I started playing tennis on the  
weekends, 
B: haven't u had that checked out yet? 
Q: What does the woman imply? 
9.A: Hi,uhm... I think something's wrong with the washing machine. It works  
and I just did my laundry but it makes some strange noises. Maybe u should  
call sb to fix it. 
B: Oh don't worry. Sb from the repair shop is already on the way over to  
take a look at it. 
Q: What does the man imply? 
10.A; It's so thoughtful of u to offer to drop me off at the train station.  
Ru sure it's not out of ur way? 
B: Not at all. The station is really close to where i'm going. 
Q:What does the man mean? 
11.A: I'm here about the job u advertised in the paper. 
B; U need one of those forms over there, on the table next to the file  
cabinet. 
Q: What does the woman imply the man should do? 
12.A:I know i ought to call home, but i've got a plane to take and I may be  
late. 
B:But it only takes a minute. 
Q: What does the woman suggest the man do? 
13.A: i have to drive in to Chicago next week. Do u have a map I could  
borrow? 
B: Sorry I don't,but i can pick one up for u while I'm at the bookstore. 
Q: What does the man mean? 
14.A:What did u think of the paintings that Ted was showing last week? 
B; I never made it to the exhibit. 
Q: What does the woman mean? 
15.A: Did u hear about the big show storm in Iowa yesterday? Three feet and  
twelve hours. 
B: Yeah, and I hear it's headed our way. We'r supposed to get the same  
thing tonight. 
Q: What does the woman mean? 
16.A:U'r joining us for dinner tonight, aren't u? 
B: Oh, I'm really sorry, but I had the wrong date for my geometry test. i  
just found out it's tomorrow and I need all the time I can get to prepare. 
Q: What does the woman imply? 
17.A: I can't decide whether I should take physics now or wait till next  
semester. 
B: U might as well get it over with if u can. 
Q: What does the man suggest the woman do? 
18.A: U look different today. Did u get a haircut?  
B: That's funny. Ur the third person to ask me that. But all I did was  
getting new frames for my eye glasses. 
Q: What does the man imply? 
19.A: Dr. Eliot, I'd like u to check the way u calculated my grade for this  
test, I think u may have made a mistake in adding up the number of  
questions I got right. When I added them up I came up with thos slightly  
higher grade than u did. 
B: I'd be ahppy to check it for u. And if I made a mistake in determing the  
grade i'll be sure to correct it. Don't worry. 
Q: What does the man imply? 
20.A: That last speaker was pretty boring. But he did make a few good  
points at the end. 
B: Really? I didn't catch them. I must have dozed off for a minute. 
Q: What does the woman neam? 
21.A: If u run into Joan this afternoon, could u ask her to call me. I need  
that book back that i lent her yesterday. 
B: No need. I saw her this morning and I've got it right here. 
Q: What can be infered from the conversation? 
22.A:I told my student today that I'd be taking a sabbatical next semester.  
But they didn't seem very surprised. 
B: Well, last week i let ur plans slip to same my students. So more than  
likely the word got around. 
Q: What does the man explain to the woman? 
23.A:Mary, I've got the bowls out for the stew. Do u think it needs any  
more pepper before I serve it? 
B: It's really quite nice an we did exactly what the recipe says. Why take  
a chance of ruining it? 
Q: What does the woman imply the man should do? 
24.A: Those were such funny stories Tom told last night. He was like a  
totally different guy. 
B: Yeah, really. He is normally so serious. What do u think brought all  
that out of him. 
Q: What does the woman imply? 
25.A: Wow,look at all these old books on this shelf. They've got to be at  
least one hundred years old. 
I'll bet they worth a lot to collectors. 
B; Well. they'be got a lot of sentimental value for me, but that's about it. 
Q: What does the man mean? 
26.A: Hi, Susan, would u like to go our to eat with us? Several of us are  
going over to the Macardy's. 
B: Well, that sure beats sticking around here. Uhh... just let me pack up  
my things. 
Q: What is the woman going to do? 
27.A: I thought u said u and ur friends were just planning a small  
gathering. I could hear u from all the way up on the fourth floor of the  
building. 
B:OH, Gee, I'm really sorry. I guess we did get a little carried away,  
didn't we? 
Q: What can be inferred from the conversations? 
28.A: I kept looking for Mary at the seminar but never did see her. I can't  
imagine she forgot about it. She'd be talking about it for weeks. 
B: Oh she didn't. It's just that she caught areally bad cold a couple of  
days ago. 
Q: What can be inferred fom the conversation? 
29.A: Our history presentation is Thursday. When do u want to get together  
to work on it? 
B: Well, how about Monday ?That way we will still have enough time to  
figure out anything we are having trouble with. 
Q: What does the woman suggest they do? 
30. A: Hi, thanks for ur help. I guess I can handle the rest myself. But  
just in case, rugoing to be around later? 
B: I don't know but u can always ask Judy. She 's really good with these  
kinds of problems. 
Q: What does the woman imply? 
Q31-34 
Hi, Janet, ur so lucky to be done with ur final exams and term papers. I  
still have 2 more finals to take? 
Really? 
Yeah, So what ru doing this summer, anything special? 
Well, actually yeah. My parents have alway liked taking my sister and me to  
different places in the United States. U know, places with historical  
significance. I guess they wanted to reinforce the stuff we learned in  
school about history. And so even though we are older now, they still do  
once in a while. Oh so where ru going this summer? 
Well, this summer it's finally goinf to be Gettyburg. 
Fianlly? U mean they never took u yet? I mean Gettysburg, it's probably the  
most famous civil war site in the country, It's only a couple of hours  
away. I think that would be one of the first place that they've taken u. i  
have been there a couple of times. 
We were gonna to go about ten,well,no, it was exactly ten years ago, but I  
don't know,sth happened, I cannot remember what. 
Sth changed ur plans? 
Yeah, don't ask me what it was, but we ended up not going anywhere that  
year. I hope that doesn't happen again this year. I wrote a paper about  
Gettysburg last semester for a history class. I was taking. Well about the  
political situation in the United States right after the battle at  
Gettysburg, So I'm eager to see the place. 
31. What are the students mainly discussing? 
32.What does the man find surprising about the woman? 
33.What is the woman unable to remember? 
34.What does the woman imply about Gettysburg? 
Q35-39 
What ru doing? 
I'm ordering somw filing cabinet out of a catalog. 
What do u need them for? 
There's so much stuff piling up in my dormitory room. If I don't do sth  
soon, I won't be able to move in there. 
Do u usually order from a catalog? 
Sometimes.Why? 
OH, it's just in the history class today we were talking about how the  
catalog sales business first got started in the US. A Chicago retailer,  
Montgomery Ward started it in the late 1800s. It was really popullar among  
farmers. it was difficult for them to make it to the big city stores so  
they ordered from catalogs. 
Was Ward the only one in the business? 
At first, but another person named richard Sears started his own catalog  
after he heard how much mmoney Ward was making. 
What made them so popular? 
Farmers trusted Ward and Sears for one thing. They delivered the products  
the farmers paid for and even refunded the price of things the farmers  
weren't satisfied with. The catalog became so popular some countries school  
teachers even used them as textbooks. 
Textbooks? 
Yeah, Students practice spelling the names and adding up the prices of  
things in the catalogs. 
Was everybody that thrilled about it? 
That's doubtful. Say they drove some small store owners out of business.  
Sears and Ward sold stuff in such large quantities. They were able to  
undercut the prices at some small family owned stores. 
35.What is the conversation mainly about? 
36. Why was the woman reading a catalog? 
37.Who were the main customers of Sears and Ward's business? 
38. What unusual way were the catalog used? 
39. What was one of the negative effects of the catalog business? 
Q40-42 
The birds u see here in this slide are peregrine falcons. These birds  
represent a success story among animals on the endanged species list.. In  
the 1970s, the peregrine falcons almost disappeared as a result of the  
contamination of the food chain by the DDT in pesticide. The presence of  
the poison in their systems resulted in eggs too weak to support the  
incubating chicks. Their remarkable recovery is a result of the ban of DDT  
as a pesticide, aggressive captive feeding programs and their own  
resiliency. the peregrine falcon is one of the fastest birds alive. They've  
been clocked at 140 to 200 miles per hour in successful pursuit of pray. In  
addition to speed, these birds fly directly into head winds and they are  
capable of flying more than 600 miles per day with favorable tail winds.  
today with the sophistication of telemetty, the speeds of these birds can  
be tracked by orbiting satellites, by means of transmitters attached to the  
bird. For example , peregrine falcons stage in warmer climate, in other  
words,they spend time in the southern US over changes preparing them to  
breed in the Arctic. Then they migrate north to the much colder Arctic  
regions. Birds have been tracked from Texas in late April to their nesting  
ground in Alaska, Canada and Greenland. Now let's move on to another  
species of birds, the bald eagles. 
40 What is the talk mainly about? 
41.According to the passage, what makes the peregrine falcon a good hunter? 
42. How did biologists track peregrine falcons over long distances? 
Q43-46 
Ok, so in our last class we were discussing big bands swing music.,u  
remember this was a kind of dance music with a steady rhythm. But today we  
deal with music played by smaller jazz bands. It's called bebop may use all  
sorts of new types of rhythms, some of them very irregular. We'll talk more  
about that later. But first I want to tald about some of the social  
elements that i believe contributed to the development of bebop music. To  
do this, we have to look at when bebop arose and started becoming so  
popular,which was from the late 1930s through the 1940s, from the time of  
the environment for bebop music was the decline of the US economy. During  
the great depression. the economy suffered tremendously. And fewer people  
had money to spend on entertainment. Then during the 2nd World War the  
government imposed a new tax on public entertainment, what u might call  
performance tax. The government collected money on performances that  
included any types of acting,dancing or singing, but not instrumental  
music. So to avoid this new tax, some jazz bands stop using singer  
altogether. They started relying on the creativity of the instrumentalist  
to attract audiences. This was what bebop bands did. Now remember a lot of  
bands have singers. So the instrumentalistssimply played in the background  
and had occasional solos while the singer sang the melody to the songs,but  
not bebop bands. So the instrumentalists had much more frredom to be  
creative. So they experimented, playing the music faster and using new  
irregular sorts of rhythms. 
43.What is the talk mainly about? 
44. How didi the bebop bands avoid the performance tax? 
45 Why does the professor mention the decline of the US economy during the  
great depression? 
46. What dose the professor describe as a significant characteristic of  
bebop music? 
Q47-50 
UR professor has asked me to talk to u today about the topic that should be  
of real concern to civil engineers: the erosion of the US beaches. Let me  
start with some statistics. Did uknow that 90% of the coast in this country  
is eroding, on the gulf of Mexico for instance, erosion averages 4 to 5  
feet per year. Over the past 20 years, there has been an increase in  
building along the coast, even though geologists and environmentalists have  
been warning communities about problems like erosion. Someway communities  
have tried to protect their building and roads and to build seawalls.  
However geologists have fould that such stabilizing structure actually  
speed up the destruction of the beaches. These beaches with seawalls,  
called stabilized beaches, are much narrower than beaches without them. U  
may wonder how seawalls speed up beach loss. The explanations is simple.If  
the flow of the beaches is gentle, the water energy is lessened as it  
washes up along the shore. It is reduced even more that returns to the sea  
so it doesn't carry back much sand. ON the other hand, when the water hit  
the nearly vertical face of the seawall. it goes straight back to the sea  
with the full force of its energy and it carries back a great deal of sand.  
Because of the real risk of losing beaches, many geologists support a ban  
on all types of stabilizing construction on shore lines. 
47. What is the speaker mainly discussing? 
48. Why do communities build seawalls? 
49. How does a gently sloping beach help prevent erosion? 
50.What would the speaker probably advise engineers to do? 

